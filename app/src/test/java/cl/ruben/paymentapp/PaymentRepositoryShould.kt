package cl.ruben.paymentapp

import cl.ruben.paymentapp.data.mappers.ModelPaymentMappers
import cl.ruben.paymentapp.data.models.BankOptionModel
import cl.ruben.paymentapp.data.models.InstallmentModel
import cl.ruben.paymentapp.data.models.InstallmentsRequestModel
import cl.ruben.paymentapp.data.models.PaymentMethodModel
import cl.ruben.paymentapp.data.repository.PaymentRepository
import cl.ruben.paymentapp.data.repository.source.remote.PaymentMethodsSource
import cl.ruben.paymentapp.domain.entities.BankEntity
import cl.ruben.paymentapp.domain.entities.InstallmentEntity
import cl.ruben.paymentapp.domain.entities.InstallmentRequestEntity
import cl.ruben.paymentapp.domain.entities.PaymentMethodEntity
import cl.ruben.paymentapp.utils.BaseUnitTest
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

class PaymentRepositoryShould : BaseUnitTest() {

    private val source: PaymentMethodsSource = mock()
    private val mapper: ModelPaymentMappers = mock()
    private val listPaymentEntity = mock<List<PaymentMethodEntity>>()
    private val listPaymentModel = mock<List<PaymentMethodModel>>()
    private val paymentId = ""

    private val listBankEntity = mock<List<BankEntity>>()
    private val listBankModel = mock<List<BankOptionModel>>()

    private val installmentRequestEntity = mock<InstallmentRequestEntity>()
    private val installmentRequestModel = mock<InstallmentsRequestModel>()
    private val listInstallmentEntity = mock<List<InstallmentEntity>>()
    private val listInstallmentModel = mock<List<InstallmentModel>>()


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun getPaymentMethodsFromSource() = runTest {

        val repository = init()
        repository.fetchPaymentMethods()
        verify(source, times(1)).fetchPaymentMethods()
        assertEquals(listPaymentEntity, repository.fetchPaymentMethods())
    }


    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun getIssuerListByPaymentId() = runTest {
        val repository = init()
        repository.fetchBankList(paymentId)
        verify(source, times(1)).fetchCardIssuers(paymentId)
        assertEquals(listBankEntity, repository.fetchBankList(paymentId))
    }

    @Test
    fun getInstallments() = runTest {
        val repository = init()
        repository.fetchInstallments(installmentRequestEntity)
        verify(source, times(1)).fetchInstallments(installmentRequestModel)

        assertEquals(
            listInstallmentEntity,
            repository.fetchInstallments(installmentRequestEntity)
        )
    }


    private suspend fun init(): PaymentRepository {
        mockMapper()
        mockSource()

        return PaymentRepository(source, mapper)

    }

    private suspend fun mockSource() {
        whenever(source.fetchPaymentMethods()).thenReturn(
            listPaymentModel
        )
        whenever(source.fetchCardIssuers(paymentId)).thenReturn(listBankModel)
        whenever(
            source.fetchInstallments(
                mapper.installmentRequestEntityToModel(
                    installmentRequestEntity
                )
            )
        ).thenReturn(
            listInstallmentModel
        )
    }

    private fun mockMapper() {
        whenever(mapper.listPaymentMethodModelToListEntity(listPaymentModel)).thenReturn(
            listPaymentEntity
        )
        whenever(mapper.listBankOptionModelToListEntity(listBankModel)).thenReturn(
            listBankEntity
        )
        whenever(
            mapper.listInstallmentModelToListInstallmentEntity(
                listInstallmentModel
            )
        ).thenReturn(
            listInstallmentEntity
        )
        whenever(mapper.installmentRequestEntityToModel(installmentRequestEntity)).thenReturn(
            installmentRequestModel
        )
    }


}