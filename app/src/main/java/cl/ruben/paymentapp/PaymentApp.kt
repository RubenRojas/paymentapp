package cl.ruben.paymentapp

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PaymentApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: PaymentApp? = null
        var currentId: Int = -1

        fun context(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context: Context = PaymentApp.context()
    }
}