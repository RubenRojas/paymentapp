package cl.ruben.paymentapp.domain.entities

data class InstallmentRequestEntity(
    val payment_id: String,
    val amount: Int,
    val issuer_id: Int,
)