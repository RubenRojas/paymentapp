package cl.ruben.paymentapp.domain.interfaces

import cl.ruben.paymentapp.domain.entities.*

interface PaymentRepositoryInterface {
    suspend fun fetchPaymentMethods(): List<PaymentMethodEntity>
    suspend fun fetchBankList(paymentId: String): List<BankEntity>
    suspend fun fetchInstallments(requestEntity: InstallmentRequestEntity): List<InstallmentEntity>
    suspend fun createLocal(data: PaymentJsonEntity): Int
    suspend fun updateProgress(id: Int, data: PaymentJsonEntity)
    suspend fun getLocalData(): List<PaymentJsonEntity>
    suspend fun getLocalElement(id: Int): PaymentJsonEntity?
    suspend fun getPaymentPendingToShow(): PaymentJsonEntity?
    suspend fun clearLocalData()
}