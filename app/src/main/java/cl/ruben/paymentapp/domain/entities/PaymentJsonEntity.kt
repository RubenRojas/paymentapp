package cl.ruben.paymentapp.domain.entities

data class PaymentJsonEntity(
    val bank: BankEntity?,
    val payment: PaymentMethodEntity?,
    val installment: InstallmentOptionEntity?,
    val amount: Int?,
    val hasDisplayed: Boolean = false
)