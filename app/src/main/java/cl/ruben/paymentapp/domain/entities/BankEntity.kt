package cl.ruben.paymentapp.domain.entities


data class BankEntity(
    val name: String,
    val thumbnail: String,
    val id: String,
)

