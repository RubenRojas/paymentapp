package cl.ruben.paymentapp.domain.entities

data class InstallmentEntity(
    val paymentMethodId: String,
    val paymentTypeId: String,
    val issuer: BankEntity,
    val options: List<InstallmentOptionEntity>
)

data class InstallmentOptionEntity(
    val id: String,
    val message: String,
    val installments: Long, // cuotas
    val installmentAmount: Double, // cantidad_cuotas
    val totalAmount: Double,  // montoTotal
    val installment_rate: Double,
    val labels: List<String>,

    )
