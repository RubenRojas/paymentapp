package cl.ruben.paymentapp.domain.usecases

import cl.ruben.paymentapp.data.repository.PaymentRepository
import cl.ruben.paymentapp.domain.entities.*
import javax.inject.Inject

class PaymentUsecase @Inject constructor(
    private val repository: PaymentRepository
) {
    suspend fun fetchPaymentMethods(): List<PaymentMethodEntity> {
        return repository.fetchPaymentMethods()
    }

    suspend fun fetchBankOptions(paymentId: String): List<BankEntity> {
        return repository.fetchBankList(paymentId)
    }

    suspend fun fetchInstallments(request: InstallmentRequestEntity): List<InstallmentEntity> {
        return repository.fetchInstallments(request)
    }

    suspend fun initLocalPaymentData(data: PaymentJsonEntity): Int {
        return repository.createLocal(data)
    }

    suspend fun updateLocalPaymentData(id: Int, data: PaymentJsonEntity) {
        repository.updateProgress(id, data)
    }

    suspend fun clearLocalData() {
        return repository.clearLocalData()
    }

    suspend fun getPaymentPendingToDisplay(): PaymentJsonEntity {
        return repository.getPaymentPendingToShow()
    }

    suspend fun getLocalPayment(id: Int): PaymentJsonEntity {
        return repository.getLocalElement(id)!!
    }
}