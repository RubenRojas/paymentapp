package cl.ruben.paymentapp.domain.entities

data class PaymentMethodEntity(
    val name: String,
    val thumbnail: String,
    val id: String,
    val minAmount: Long,
    val maxAmount: Long,
    val paymentTypeId: String,
)