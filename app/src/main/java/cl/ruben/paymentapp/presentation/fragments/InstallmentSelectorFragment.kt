package cl.ruben.paymentapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import cl.ruben.paymentapp.databinding.FragmentInstallmentSelectorBinding
import cl.ruben.paymentapp.infraestructure.network.Status
import cl.ruben.paymentapp.presentation.adapters.InstallmentSelectorRecyclerViewAdapter
import cl.ruben.paymentapp.presentation.dialogs.SimpleAlertDialog
import cl.ruben.paymentapp.presentation.viewmodels.InstallmentSelectorViewModel
import cl.ruben.paymentapp.presentation.viewmodels.InstallmentSelectorViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InstallmentSelectorFragment : Fragment() {

    lateinit var binding: FragmentInstallmentSelectorBinding

    @Inject
    lateinit var factory: InstallmentSelectorViewModelFactory
    lateinit var viewModel: InstallmentSelectorViewModel


    lateinit var adapter: InstallmentSelectorRecyclerViewAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInstallmentSelectorBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )
        initViewModel()

        binding.nextStepButton.setOnClickListener {
            goNext()
        }
        viewModel.getInstallments()
            .observe(this as LifecycleOwner) { response ->
                run {
                    when (response.status) {
                        Status.SUCCESS -> {
                            binding.loader.root.visibility = View.GONE
                            adapter =
                                InstallmentSelectorRecyclerViewAdapter(
                                    response.data!!
                                ) { presenter ->
                                    viewModel.setSelectedInstallment(presenter)
                                }

                            binding.installmentList.adapter = adapter
                        }
                        Status.ERROR -> {
                            binding.loader.root.visibility = View.GONE
                        }
                        Status.LOADING -> {
                            binding.loader.root.visibility = View.VISIBLE
                        }
                    }
                }
            }



        return binding.root
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            factory
        )[InstallmentSelectorViewModel::class.java]
    }

    private fun goNext() {
        if (viewModel.validateSelected()) {

            viewModel.updateData().observe(this as LifecycleOwner) {
                val action: NavDirections =
                    InstallmentSelectorFragmentDirections.actionInstallmentSelectorToAmountInput()

                findNavController().navigate(action)
            }

        } else {
            context?.let {
                SimpleAlertDialog.show(
                    it,
                    "Ocurrio un Error",
                    "Debes seleccionar un elemento"
                )
            }
        }

    }

}