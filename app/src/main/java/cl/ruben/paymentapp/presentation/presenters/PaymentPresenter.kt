package cl.ruben.paymentapp.presentation.presenters

import cl.ruben.paymentapp.domain.entities.BankEntity
import cl.ruben.paymentapp.domain.entities.InstallmentOptionEntity
import cl.ruben.paymentapp.domain.entities.PaymentMethodEntity

data class PaymentPresenter(
    val bank: BankEntity?,
    val payment: PaymentMethodEntity?,
    val installment: InstallmentOptionEntity?,
    val amount: Int?,
    val hasDisplayed: Boolean
)