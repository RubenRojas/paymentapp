package cl.ruben.paymentapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import cl.ruben.paymentapp.databinding.FragmentBankSelectorBinding
import cl.ruben.paymentapp.infraestructure.network.Status
import cl.ruben.paymentapp.presentation.adapters.BankSelectorRecyclerViewAdapter
import cl.ruben.paymentapp.presentation.dialogs.SimpleAlertDialog
import cl.ruben.paymentapp.presentation.viewmodels.BankSelectorViewModel
import cl.ruben.paymentapp.presentation.viewmodels.BankSelectorViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BankSelectorFragment : Fragment() {
    lateinit var binding: FragmentBankSelectorBinding

    @Inject
    lateinit var bankSelectorViewModelFactory: BankSelectorViewModelFactory
    lateinit var viewModel: BankSelectorViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBankSelectorBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )

        initViewModel()
        initView()


        binding.nextStepButton.setOnClickListener {
            goNext()
        }
        return binding.root
    }

    private fun initView() {

        viewModel.getBankList()
            .observe(this as LifecycleOwner) { response ->
                when (response.status) {
                    Status.SUCCESS -> {
                        binding.loader.root.visibility = View.GONE

                        binding.bankList.adapter =
                            BankSelectorRecyclerViewAdapter(
                                response.data!!
                            ) { presenter ->
                                viewModel.setSelectedBank(presenter)
                            }
                    }
                    Status.ERROR -> {
                        binding.loader.root.visibility = View.GONE
                    }
                    Status.LOADING -> {
                        binding.loader.root.visibility = View.VISIBLE
                    }
                }
            }

    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            bankSelectorViewModelFactory
        )[BankSelectorViewModel::class.java]
    }

    private fun goNext() {

        if (viewModel.validateSelected()) {
            viewModel.updateData().observe(this as LifecycleOwner) {
                val action: NavDirections =
                    BankSelectorFragmentDirections.actionBankSelectorToInstallmentSelector()

                findNavController().navigate(action)
            }
        } else {
            context?.let {
                SimpleAlertDialog.show(
                    it,
                    "Ocurrio un error",
                    "Debes seleccionar un banco"
                )
            }
        }


    }

}