package cl.ruben.paymentapp.presentation.presenters



data class PaymentMethodPresenter(
    val name: String,
    val thumbnail: String,
    val id: String,
    val minAmount: Long,
    val maxAmount: Long,
    val paymentTypeId: String,
)