package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.domain.usecases.PaymentUsecase
import cl.ruben.paymentapp.infraestructure.default_error_message
import cl.ruben.paymentapp.infraestructure.network.CustomResponse
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import cl.ruben.paymentapp.presentation.presenters.AmountInputPresenter
import kotlinx.coroutines.Dispatchers

class AmountInputViewModel(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    fun saveAmount(presenter: AmountInputPresenter) = liveData(Dispatchers.IO) {
        PaymentApp.currentId = usecase.initLocalPaymentData(
            mapper.amountInputPresenterToPaymentJsonEntity(presenter)
        )
        emit(true)
    }

    fun validateInput(text: String): Boolean {
        return text.isNotBlank() && text.isNotEmpty()
    }

    fun checkElementToDisplay() = liveData(Dispatchers.IO) {
        try {
            val entity = usecase.getPaymentPendingToDisplay()
            emit(
                CustomResponse.success(
                    mapper.paymentJsonEntityToPresenter(
                        entity
                    )
                )
            )
        } catch (e: Exception) {
            emit(CustomResponse.error(e.message ?: default_error_message))
        }
    }

    fun clearLocalPayments() = liveData(Dispatchers.IO) {
        try {
            usecase.clearLocalData()

        } catch (_: Exception) {

        }
        emit(true)
    }

}