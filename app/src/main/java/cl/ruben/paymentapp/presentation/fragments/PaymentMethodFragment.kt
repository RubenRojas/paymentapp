package cl.ruben.paymentapp.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import cl.ruben.paymentapp.databinding.FragmentPaymentMethodBinding
import cl.ruben.paymentapp.infraestructure.network.Status
import cl.ruben.paymentapp.presentation.adapters.PaymentItemRecyclerViewAdapter
import cl.ruben.paymentapp.presentation.dialogs.SimpleAlertDialog
import cl.ruben.paymentapp.presentation.presenters.PaymentMethodPresenter
import cl.ruben.paymentapp.presentation.viewmodels.PaymentMethodViewModel
import cl.ruben.paymentapp.presentation.viewmodels.PaymentMethodViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class PaymentMethodFragment : Fragment() {
    lateinit var binding: FragmentPaymentMethodBinding

    @Inject
    lateinit var paymentMethodViewModelFactory: PaymentMethodViewModelFactory
    private lateinit var viewModel: PaymentMethodViewModel

    private val clickListener = { presenter: PaymentMethodPresenter ->
        viewModel.setSelectedPaymentMethod(presenter)
    }
    lateinit var adapter: PaymentItemRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPaymentMethodBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )
        initViewModel()
        initView();

        viewModel.getPaymentMethods().observe(this as LifecycleOwner) { it ->
            when (it.status) {
                Status.SUCCESS -> {
                    binding.loader.root.visibility = View.GONE
                    adapter = PaymentItemRecyclerViewAdapter(
                        it.data!!, clickListener
                    )
                    binding.paymentList.adapter = adapter

                    initView()

                }
                Status.ERROR -> {
                    binding.loader.root.visibility = View.GONE
                }
                Status.LOADING -> {
                    binding.loader.root.visibility = View.VISIBLE
                }
            }
        }


        binding.nextStepButton.setOnClickListener {
            goNext()
        }
        return binding.root
    }


    private fun initView() {
        if (viewModel.validateSelected()) {
            adapter.setSelection(viewModel.selectedPaymentMethodPresenter)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            paymentMethodViewModelFactory
        )[PaymentMethodViewModel::class.java]
    }

    private fun goNext() {
        if (viewModel.validateSelected()) {

            viewModel.updateData().observe(this as LifecycleOwner) {
                val action: NavDirections =
                    PaymentMethodFragmentDirections.actionPaymentMethodToBankSelector()

                findNavController().navigate(action)
            }


        } else {
            context?.let {
                SimpleAlertDialog.show(
                    it,
                    "Ocurrio un error",
                    "Debes seleccionar una forma de pago"
                )
            }
        }

    }

}