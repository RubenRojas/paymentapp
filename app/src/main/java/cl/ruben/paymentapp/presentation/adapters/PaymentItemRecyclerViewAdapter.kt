package cl.ruben.paymentapp.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import cl.ruben.paymentapp.databinding.ItemLayoutPaymentMethodBinding
import cl.ruben.paymentapp.presentation.presenters.PaymentMethodPresenter
import com.squareup.picasso.Picasso

class PaymentItemRecyclerViewAdapter(
    private val items: List<PaymentMethodPresenter>,
    private val listener: (PaymentMethodPresenter) -> Unit,
) : RecyclerView.Adapter<PaymentItemRecyclerViewAdapter.ViewHolder>() {

    private var selectedPosition = -1

    inner class ViewHolder(binding: ItemLayoutPaymentMethodBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val itemName = binding.paymentItemName
        val itemImage = binding.paymentItemImage
        val radio: RadioButton = binding.paymentItemRadio
        val container = binding.paymentItemContainer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            ItemLayoutPaymentMethodBinding.inflate(
                LayoutInflater.from(
                    parent.context,
                ), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.radio.isChecked = position == selectedPosition
        holder.itemName.text = item.name
        holder.itemName.setOnClickListener {
            setCheckedItem(item, holder)
        }
        holder.radio.setOnClickListener {
            setCheckedItem(item, holder)
        }
        Picasso.get().load(item.thumbnail).into(holder.itemImage)

    }

    fun setSelection(item: PaymentMethodPresenter) {
        val index = items.indexOf(item)
        selectedPosition = index
        notifyItemChanged(selectedPosition)
    }

    private fun setCheckedItem(
        item: PaymentMethodPresenter,
        holder: ViewHolder
    ) {
        listener(item)
        val indexCopy = selectedPosition
        selectedPosition = holder.absoluteAdapterPosition

        notifyItemChanged(indexCopy)
        notifyItemChanged(selectedPosition)
    }

    override fun getItemCount(): Int {
        return items.size
    }
}