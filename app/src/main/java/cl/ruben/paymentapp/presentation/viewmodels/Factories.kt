package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import cl.ruben.paymentapp.domain.usecases.PaymentUsecase
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import javax.inject.Inject

class PaymentMethodViewModelFactory @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PaymentMethodViewModel(usecase, mapper) as T
    }
}

class BankSelectorViewModelFactory @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return BankSelectorViewModel(usecase, mapper) as T
    }
}

class InstallmentSelectorViewModelFactory @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return InstallmentSelectorViewModel(usecase, mapper) as T
    }
}

class AmountInputViewModelFactory @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AmountInputViewModel(usecase, mapper) as T
    }
}