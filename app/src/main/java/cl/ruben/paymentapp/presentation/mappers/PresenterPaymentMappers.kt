package cl.ruben.paymentapp.presentation.mappers

import cl.ruben.paymentapp.domain.entities.*
import cl.ruben.paymentapp.presentation.presenters.*

class PresenterPaymentMappers {
    fun paymentMethodEntityToPresenter(entity: PaymentMethodEntity): PaymentMethodPresenter {
        return PaymentMethodPresenter(
            entity.name,
            entity.thumbnail,
            entity.id,
            entity.minAmount,
            entity.maxAmount,
            entity.paymentTypeId,
        )
    }

    fun bankEntityToPresenter(entity: BankEntity): BankPresenter {
        return BankPresenter(
            entity.id,
            entity.name,
            entity.thumbnail
        )
    }

    fun installmentRequestPresenterToEntity(
        presenter: InstallmentRequestPresenter
    ): InstallmentRequestEntity {
        return InstallmentRequestEntity(
            presenter.payment_id,
            presenter.amount,
            presenter.issuer_id
        )
    }

    fun installmentEntityToListInstallmentPresenter(
        entity: InstallmentEntity
    ): List<InstallmentPresenter> {
        return entity.options.map { p ->
            InstallmentPresenter(
                p.id,
                p.message,
                p.installments,
                p.installmentAmount,
                p.totalAmount,
                p.installment_rate,
                p.labels,
            )
        }
    }

    fun amountInputPresenterToPaymentJsonEntity(
        presenter: AmountInputPresenter
    ): PaymentJsonEntity {
        val amount: Int = presenter.toNumber()
        return PaymentJsonEntity(
            null, null, null, amount
        )

    }

    fun bankPresenterToEntity(presenter: BankPresenter): BankEntity {
        return BankEntity(
            presenter.name,
            presenter.thumbnail,
            presenter.id
        )
    }

    fun paymentMethodPresenterToEntity(presenter: PaymentMethodPresenter): PaymentMethodEntity {
        return PaymentMethodEntity(
            presenter.name,
            presenter.thumbnail,
            presenter.id,
            presenter.minAmount,
            presenter.maxAmount,
            presenter.paymentTypeId
        )
    }

    fun installmentPresenterToEntity(p: InstallmentPresenter): InstallmentOptionEntity {
        return InstallmentOptionEntity(
            p.id,
            p.message,
            p.installments,
            p.installmentAmount,
            p.totalAmount,
            p.installment_rate,
            p.labels,
        )
    }

    fun paymentJsonEntityToPresenter(e: PaymentJsonEntity): PaymentPresenter {
        return PaymentPresenter(
            e.bank,
            e.payment,
            e.installment,
            e.amount,
            e.hasDisplayed,
        )
    }

}