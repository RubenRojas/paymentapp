package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.domain.usecases.PaymentUsecase
import cl.ruben.paymentapp.infraestructure.default_error_message
import cl.ruben.paymentapp.infraestructure.network.CustomResponse
import cl.ruben.paymentapp.infraestructure.requiredPaymentTypeId
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import cl.ruben.paymentapp.presentation.presenters.PaymentMethodPresenter
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


class PaymentMethodViewModel @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    lateinit var selectedPaymentMethodPresenter: PaymentMethodPresenter

    fun getPaymentMethods() = liveData(Dispatchers.IO) {
        emit(CustomResponse.loading())
        try {
            val entities = usecase.fetchPaymentMethods()
            val methods = entities
                .filter { it.paymentTypeId == requiredPaymentTypeId }
                .map { mapper.paymentMethodEntityToPresenter(it) }
            emit(CustomResponse.success(methods))
        } catch (e: Exception) {
            emit(CustomResponse.error(e.message ?: default_error_message))
        }
    }

    fun setSelectedPaymentMethod(presenter: PaymentMethodPresenter) {
        selectedPaymentMethodPresenter = presenter
    }

    fun validateSelected(): Boolean {
        return this::selectedPaymentMethodPresenter.isInitialized
    }

    fun updateData() = liveData(Dispatchers.IO) {
        try {
            val entity = usecase.getLocalPayment(PaymentApp.currentId)
            val updatedEntity = entity.copy(
                payment = mapper.paymentMethodPresenterToEntity(
                    selectedPaymentMethodPresenter
                )
            )
            usecase.updateLocalPaymentData(PaymentApp.currentId, updatedEntity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        emit(true)
    }

}