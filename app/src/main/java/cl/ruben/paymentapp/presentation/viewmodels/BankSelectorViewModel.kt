package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.domain.usecases.PaymentUsecase
import cl.ruben.paymentapp.infraestructure.default_error_message
import cl.ruben.paymentapp.infraestructure.network.CustomResponse
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import cl.ruben.paymentapp.presentation.presenters.BankPresenter
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class BankSelectorViewModel @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    lateinit var selectedBankPresenter: BankPresenter
    fun getBankList() = liveData(Dispatchers.IO) {
        emit(CustomResponse.loading())
        try {
            val data = usecase.getLocalPayment(PaymentApp.currentId)
            val id = data.payment?.id
            val entities = usecase.fetchBankOptions(id!!)
            emit(CustomResponse.success(entities.map {
                mapper.bankEntityToPresenter(
                    it
                )
            }))
        } catch (e: Exception) {
            emit(CustomResponse.error(e.message ?: default_error_message))
        }
    }

    fun setSelectedBank(presenter: BankPresenter) {
        selectedBankPresenter = presenter
    }

    fun validateSelected(): Boolean {
        return this::selectedBankPresenter.isInitialized
    }

    fun updateData() = liveData(Dispatchers.IO) {
        try {
            val entity = usecase.getLocalPayment(PaymentApp.currentId)
            val updatedEntity = entity.copy(
                bank = mapper.bankPresenterToEntity(selectedBankPresenter)
            )
            usecase.updateLocalPaymentData(PaymentApp.currentId, updatedEntity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        emit(true)
    }


}
