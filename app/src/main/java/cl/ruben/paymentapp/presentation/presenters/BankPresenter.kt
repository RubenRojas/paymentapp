package cl.ruben.paymentapp.presentation.presenters

data class BankPresenter(
    val id: String,
    val name: String,
    val thumbnail: String,
)
