package cl.ruben.paymentapp.presentation.dialogs

import android.app.AlertDialog
import android.content.Context

class SimpleAlertDialog {
    companion object {
        fun show(ctx: Context, title: String, message: String) {
            val builder = AlertDialog.Builder(ctx)
            builder.setTitle(title)
            builder.setMessage(message)
            builder.setIcon(android.R.drawable.ic_dialog_alert)
            builder.setPositiveButton("Ok") { _, _ ->
            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }

}