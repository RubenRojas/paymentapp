package cl.ruben.paymentapp.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.domain.usecases.PaymentUsecase
import cl.ruben.paymentapp.infraestructure.default_error_message
import cl.ruben.paymentapp.infraestructure.network.CustomResponse
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import cl.ruben.paymentapp.presentation.presenters.InstallmentPresenter
import cl.ruben.paymentapp.presentation.presenters.InstallmentRequestPresenter
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class InstallmentSelectorViewModel @Inject constructor(
    private val usecase: PaymentUsecase,
    private val mapper: PresenterPaymentMappers,
) : ViewModel() {

    lateinit var selectedInstallmentPresenter: InstallmentPresenter


    fun setSelectedInstallment(presenter: InstallmentPresenter) {
        selectedInstallmentPresenter = presenter
    }

    fun validateSelected(): Boolean {
        return this::selectedInstallmentPresenter.isInitialized
    }

    fun updateData() = liveData(Dispatchers.IO) {
        try {
            val entity = usecase.getLocalPayment(PaymentApp.currentId)
            val updatedEntity = entity.copy(
                installment = mapper.installmentPresenterToEntity(
                    selectedInstallmentPresenter
                )
            )
            usecase.updateLocalPaymentData(PaymentApp.currentId, updatedEntity)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        emit(true)
    }


    fun getInstallments() =
        liveData(Dispatchers.IO) {
            emit(CustomResponse.loading())
            try {

                val data = usecase.getLocalPayment(PaymentApp.currentId)
                val request = InstallmentRequestPresenter(
                    data.payment!!.id,
                    data.amount!!,
                    data.bank!!.id.toInt()
                )

                val entities = usecase.fetchInstallments(
                    mapper.installmentRequestPresenterToEntity(request)
                )
                emit(
                    CustomResponse.success(
                        mapper.installmentEntityToListInstallmentPresenter(
                            entities.first()
                        )
                    )
                )
            } catch (e: Exception) {
                emit(CustomResponse.error(e.message ?: default_error_message))
            }
        }
}