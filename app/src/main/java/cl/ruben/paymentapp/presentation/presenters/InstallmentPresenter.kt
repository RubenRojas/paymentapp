package cl.ruben.paymentapp.presentation.presenters

data class InstallmentPresenter(
    val id: String,
    val message: String,
    val installments: Long, // cuotas
    val installmentAmount: Double, // cantidad_cuotas
    val totalAmount: Double,  // montoTotal
    val installment_rate: Double,
    val labels: List<String>,
) {
    fun formatLabels(): String {
        val label = labels.last();
        val elements = label.split("|")
        if (elements.isNotEmpty()) {
            return elements.joinToString(" ") { it.replace("_", " - ") }
        }
        return ""
    }
}


data class InstallmentRequestPresenter(
    val payment_id: String,
    val amount: Int,
    val issuer_id: Int,
)