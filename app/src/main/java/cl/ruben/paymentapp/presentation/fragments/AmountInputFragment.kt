package cl.ruben.paymentapp.presentation.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import cl.ruben.paymentapp.databinding.FragmentAmountInputBinding
import cl.ruben.paymentapp.infraestructure.TAG
import cl.ruben.paymentapp.infraestructure.network.Status
import cl.ruben.paymentapp.presentation.dialogs.LastPaymentDialog
import cl.ruben.paymentapp.presentation.dialogs.SimpleAlertDialog
import cl.ruben.paymentapp.presentation.extensions.NumberTextWatcher
import cl.ruben.paymentapp.presentation.presenters.AmountInputPresenter
import cl.ruben.paymentapp.presentation.viewmodels.AmountInputViewModel
import cl.ruben.paymentapp.presentation.viewmodels.AmountInputViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AmountInputFragment : Fragment() {

    lateinit var binding: FragmentAmountInputBinding

    @Inject
    lateinit var factory: AmountInputViewModelFactory
    lateinit var viewModel: AmountInputViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAmountInputBinding.inflate(
            LayoutInflater.from(
                requireContext()
            ), container, false
        )

        initViewModel()
        initView()

        binding.nextStepButton.setOnClickListener {
            goNext()
        }

        return binding.root
    }

    private fun initView() {

        binding.amountInput.addTextChangedListener(NumberTextWatcher(binding.amountInput))

        viewModel.checkElementToDisplay()
            .observe(this as LifecycleOwner) { response ->
                Log.d(TAG, response.toString())
                run {
                    when (response.status) {
                        Status.SUCCESS -> {
                            context?.let {
                                LastPaymentDialog.show(
                                    it,
                                    response.data!!
                                )
                            }
                            viewModel.clearLocalPayments()
                                .observe(this as LifecycleOwner) {}
                        }
                        Status.ERROR -> {

                        }
                        Status.LOADING -> {

                        }
                    }
                }

            }
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(
            this,
            factory
        )[AmountInputViewModel::class.java]
    }

    private fun goNext() {
        val input = binding.amountInput.text.toString()
        if (!viewModel.validateInput(input)) {
            context?.let {
                SimpleAlertDialog.show(
                    it,
                    "Error",
                    "Debes ingresar un monto"
                )
            }
        } else {
            viewModel.saveAmount(AmountInputPresenter(input))
                .observe(this as LifecycleOwner) {
                    val action: NavDirections =
                        AmountInputFragmentDirections.actionAmountInputToPaymentMethod()

                    findNavController().navigate(action)
                }

        }


    }

}