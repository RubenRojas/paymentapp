package cl.ruben.paymentapp.presentation.dialogs

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import android.widget.Button
import android.widget.TextView
import cl.ruben.paymentapp.R
import cl.ruben.paymentapp.presentation.presenters.PaymentPresenter


class LastPaymentDialog {
    companion object {
        fun show(context: Context, data: PaymentPresenter) {


            val width = context.resources.displayMetrics.widthPixels * .9
            val height = context.resources.displayMetrics.heightPixels * .7

            val dialog = Dialog(context)
            dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(
                LayoutInflater.from(context)
                    .inflate(R.layout.dialog_payment_element, null)
            )

            dialog.window?.setLayout(width.toInt(), height.toInt())


            dialog.findViewById<TextView>(R.id.payment_dialog_amount).text =
                data.amount.toString()
            dialog.findViewById<TextView>(R.id.payment_dialog_bank_name).text =
                data.bank!!.name

            dialog.findViewById<TextView>(R.id.payment_dialog_installment_message).text =
                data.installment!!.message
            dialog.findViewById<TextView>(R.id.payment_dialog_payment_name).text =
                data.payment!!.name


            (dialog.findViewById<Button>(R.id.close_button)).setOnClickListener {
                dialog.hide()
            }

            dialog.setCancelable(false)
            dialog.show()
        }
    }


}