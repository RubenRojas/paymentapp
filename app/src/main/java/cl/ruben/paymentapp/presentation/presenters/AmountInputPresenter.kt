package cl.ruben.paymentapp.presentation.presenters

data class AmountInputPresenter(
    val amount : String,
){
    fun toNumber(): Int {
        var input = amount
        input = input.replace(",", "")
        return try {
            input.toInt()
        } catch (e: Exception) {
            e.printStackTrace()
            0
        }
    }

}