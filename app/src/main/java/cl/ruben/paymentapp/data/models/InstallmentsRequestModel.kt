package cl.ruben.paymentapp.data.models

data class InstallmentsRequestModel(
    val payment_id: String,
    val amount: Int,
    val issuer_id: Int,
)