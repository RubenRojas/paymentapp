package cl.ruben.paymentapp.data.repository.source.remote

import cl.ruben.paymentapp.data.models.BankOptionModel
import cl.ruben.paymentapp.data.models.InstallmentModel
import cl.ruben.paymentapp.data.models.InstallmentsRequestModel
import cl.ruben.paymentapp.data.models.PaymentMethodModel
import cl.ruben.paymentapp.data.repository.source.remote.api.PaymentMethodsApi
import javax.inject.Inject

class PaymentMethodsSource @Inject constructor(
    private val api : PaymentMethodsApi
) {
    suspend fun fetchPaymentMethods(): List<PaymentMethodModel> {
        return api.getPaymentMethods()
    }

    suspend fun fetchCardIssuers(methodId: String): List<BankOptionModel> {
        return api.getCardIssuers(methodId)
    }

    suspend fun fetchInstallments(request: InstallmentsRequestModel): List<InstallmentModel> {
        return api.getInstallments(
            request.payment_id,
            request.amount,
            request.issuer_id
        )
    }

}