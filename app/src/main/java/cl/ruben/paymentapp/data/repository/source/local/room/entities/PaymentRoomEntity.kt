package cl.ruben.paymentapp.data.repository.source.local.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PaymentRoomEntity(
    @PrimaryKey val id: Int,
    val data: String,
)