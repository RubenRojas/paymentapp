package cl.ruben.paymentapp.data.models

data class BankOptionModel (
    val id: String,
    val name: String,
    val secure_thumbnail: String,
    val thumbnail: String?,
    val processing_mode: String,
    val status: String,
    val merchant_account_id: Any? = null
)
