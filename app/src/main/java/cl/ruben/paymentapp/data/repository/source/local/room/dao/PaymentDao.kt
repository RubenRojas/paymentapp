package cl.ruben.paymentapp.data.repository.source.local.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cl.ruben.paymentapp.data.repository.source.local.room.entities.PaymentRoomEntity

@Dao
interface PaymentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addPayment(vararg payment: PaymentRoomEntity)

    @Query("SELECT * from PaymentRoomEntity")
    suspend fun getPayments(): List<PaymentRoomEntity>

    @Query("DELETE FROM PaymentRoomEntity")
    suspend fun clearPayments()

    @Query("SELECT max(id) from PaymentRoomEntity")
    suspend fun getLastId(): Int?

    @Query("SELECT * FROM PaymentRoomEntity where id = :id")
    suspend fun getPayment(vararg id: Int): PaymentRoomEntity
}