package cl.ruben.paymentapp.data.repository.source.remote.api

import cl.ruben.paymentapp.data.models.BankOptionModel
import cl.ruben.paymentapp.data.models.InstallmentModel
import cl.ruben.paymentapp.data.models.PaymentMethodModel
import retrofit2.http.GET
import retrofit2.http.Query

interface PaymentMethodsApi {
    @GET("v1/payment_methods/")
    suspend fun getPaymentMethods(
    ) : List<PaymentMethodModel>

    @GET("v1/payment_methods/card_issuers")
    suspend fun getCardIssuers(
        @Query("payment_method_id") paymentId : String,
    ) : List<BankOptionModel>

    @GET("v1/payment_methods/installments")
    suspend fun getInstallments(
        @Query("payment_method_id") paymentId: String,
        @Query("amount") amount: Int,
        @Query("issuer.id") issuerId: Int,
    ): List<InstallmentModel>
}