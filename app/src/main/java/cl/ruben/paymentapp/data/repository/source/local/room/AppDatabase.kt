package cl.ruben.paymentapp.data.repository.source.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import cl.ruben.paymentapp.data.repository.source.local.room.dao.PaymentDao
import cl.ruben.paymentapp.data.repository.source.local.room.entities.PaymentRoomEntity
import cl.ruben.paymentapp.infraestructure.databaseVersion

@Database(entities = [PaymentRoomEntity::class], version = databaseVersion)
abstract class AppDatabase : RoomDatabase() {
    abstract fun PaymentDao(): PaymentDao
}