package cl.ruben.paymentapp.data.models

data class InstallmentModel(
    val payment_method_id: String,
    val payment_type_id: String,
    val issuer: IssuerModel,
    val processing_mode: String,
    val merchant_account_id: Any? = null,
    val payer_costs: List<InstallmentOptionModel>,
    val agreements: Any? = null
)

data class IssuerModel(
    val id: String,
    val name: String,
    val secure_thumbnail: String,
    val thumbnail: String?
)

data class InstallmentOptionModel(
    val installments: Long,
    val installment_rate: Double,
    val discount_rate: Long,
    val reimbursement_rate: Any? = null,
    val labels: List<String>,
    val installment_rate_collector: List<String>,
    val min_allowed_amount: Long,
    val max_allowed_amount: Long,
    val recommended_message: String,
    val installment_amount: Double,
    val total_amount: Double,
    val payment_method_option_id: String
)
