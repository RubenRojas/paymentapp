package cl.ruben.paymentapp.data.mappers

import cl.ruben.paymentapp.data.repository.source.local.room.entities.PaymentRoomEntity
import cl.ruben.paymentapp.domain.entities.PaymentJsonEntity
import com.google.gson.Gson

class RoomEntitiesPaymentMappers {
    fun paymentJsonEntityFromJson(json: String): PaymentJsonEntity? {
        return Gson().fromJson(
            json,
            PaymentJsonEntity::class.java
        )
    }

    fun listPaymentRoomEntityToEntity(list: List<PaymentRoomEntity>): List<PaymentJsonEntity> {
        return list.map { paymentJsonEntityFromJson(it.data)!! }
    }

    fun parseDataToJson(entity: PaymentJsonEntity): String {
        return Gson().toJson(entity)
    }
}