package cl.ruben.paymentapp.data.repository.source.local

import cl.ruben.paymentapp.data.repository.source.local.room.dao.PaymentDao
import cl.ruben.paymentapp.data.repository.source.local.room.entities.PaymentRoomEntity
import javax.inject.Inject

class PaymentMethodsLocalSource @Inject constructor(
    private val dao: PaymentDao,
) {
    suspend fun createPayment(json: String): Int {
        var nId = dao.getLastId()
        if (nId == null) nId = 1 else nId++
        dao.addPayment(
            PaymentRoomEntity(
                nId,
                json
            )
        )
        return nId;
    }

    suspend fun saveProgress(id: Int, json: String) {
        dao.addPayment(
            PaymentRoomEntity(
                id,
                json
            )
        )
    }

    suspend fun getPayment(id: Int): PaymentRoomEntity {
        return dao.getPayment(id)
    }

    suspend fun fetchPayments(): List<PaymentRoomEntity> {
        return dao.getPayments()
    }

    suspend fun clearPayments() {
        dao.clearPayments()
    }
}