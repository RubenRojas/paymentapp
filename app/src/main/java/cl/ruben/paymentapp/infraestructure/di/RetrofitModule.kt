package cl.ruben.paymentapp.infraestructure.di

import android.content.pm.PackageManager
import cl.ruben.paymentapp.PaymentApp
import cl.ruben.paymentapp.data.repository.source.remote.api.PaymentMethodsApi
import cl.ruben.paymentapp.infraestructure.baseUrl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(FragmentComponent::class)
class RetrofitModule {
    private val ctx = PaymentApp.context()
    private val key = ctx.packageManager.getApplicationInfo(ctx.packageName, PackageManager.GET_META_DATA).metaData.getString("mercadoPagoApiKey")

    private val logging =
        HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
    private val client = OkHttpClient.Builder().addInterceptor(logging)
        .addInterceptor(Interceptor{
            val original = it.request()
            val origUrl = original.url
            val nUrl = origUrl.newBuilder().addQueryParameter("public_key", key).build()
            return@Interceptor it.proceed(original.newBuilder().url(nUrl).build())
        })
        .build()



    @Provides
    fun paymentMethodApi(retrofit: Retrofit): PaymentMethodsApi =
        retrofit.create(PaymentMethodsApi::class.java)

    @Provides
    fun retrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}