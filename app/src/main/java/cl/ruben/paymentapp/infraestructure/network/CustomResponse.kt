package cl.ruben.paymentapp.infraestructure.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING,
}

data class CustomResponse<out T>(val status: Status, val data: T?) {
    companion object {
        fun <T> success(data: T): CustomResponse<T> =
            CustomResponse(status = Status.SUCCESS, data = data)
        fun <T> error(message: String): CustomResponse<T> =
            CustomResponse(status = Status.ERROR, data = message as T)
        fun <T> loading(): CustomResponse<T> = CustomResponse(status = Status.LOADING, data = null)
    }
}