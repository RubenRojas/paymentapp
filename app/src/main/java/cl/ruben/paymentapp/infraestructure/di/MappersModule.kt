package cl.ruben.paymentapp.infraestructure.di

import cl.ruben.paymentapp.data.mappers.ModelPaymentMappers
import cl.ruben.paymentapp.data.mappers.RoomEntitiesPaymentMappers
import cl.ruben.paymentapp.presentation.mappers.PresenterPaymentMappers
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
class MappersModule {
    @Provides
    fun provideModelPaymentMappers(): ModelPaymentMappers {
        return ModelPaymentMappers()
    }

    @Provides
    fun providePresenterPaymentMappers(): PresenterPaymentMappers {
        return PresenterPaymentMappers()
    }

    @Provides
    fun provideRoomEntitiesPaymentMappers(): RoomEntitiesPaymentMappers {
        return RoomEntitiesPaymentMappers()
    }
}