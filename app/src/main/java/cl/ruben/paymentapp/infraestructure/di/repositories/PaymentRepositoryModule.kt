package cl.ruben.paymentapp.infraestructure.di.repositories

import cl.ruben.paymentapp.data.repository.PaymentRepository
import cl.ruben.paymentapp.domain.interfaces.PaymentRepositoryInterface
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@Module
@InstallIn(FragmentComponent::class)
abstract class PaymentRepositoryModule {
    @Binds
    abstract fun bindPaymentRepository(
        searchEngineRepository: PaymentRepository
    ): PaymentRepositoryInterface
}